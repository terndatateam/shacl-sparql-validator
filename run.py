import time
import logging

from rdflib import Graph
from pyspark import SparkContext
from pyspark.sql import SparkSession
from spark_etl_utils import ListAccumulator

import settings
from shacl_sparql_validator import get_uris, get_target_classes, EmptySPARQLResultException
from shacl_sparql_validator.utils import validate_shapes, process_shapes
from worker import job

# Set logging for PySpark driver.
logging.basicConfig(level=getattr(logging, settings.SSV_SPARK_LOG_LEVEL))
logger = logging.getLogger('shacl.pyspark.driver')


if __name__ == '__main__':
    starttime = time.time()
    horizontal_line_length = settings.SSV_HORIZONTAL_LINE_LENGTH

    sc = SparkContext()
    sc.setLogLevel(settings.SSV_SPARK_LOG_LEVEL)
    spark = SparkSession\
        .builder\
        .master(settings.SSV_SPARK_MASTER)\
        .appName(settings.SSV_SPARK_APP_NAME)\
        .config('spark.driver.memory', settings.SSV_SPARK_DRIVER_MEMORY)\
        .getOrCreate()

    job_results = spark.sparkContext.accumulator([], ListAccumulator())

    skip_classes = settings.SSV_SKIP_CLASSES
    logger.info(f'Skip classes set to {skip_classes}')
    logger.info(f'SPARQL query LIMIT set to {settings.SSV_SPARQL_QUERY_LIMIT}')

    shapes_graph = Graph()
    for shape in settings.SSV_SHACL_SHAPES:
        shapes_graph.parse(shape, format=settings.SSV_SHACL_SHAPES_FORMAT)
    process_shapes(shapes_graph)
    validate_shapes(shapes_graph)

    classes_with_no_data = list()
    classes_with_http_errors = list()

    target_classes = get_target_classes(shapes_graph)

    for target_class in target_classes:
        logger.info('#' * horizontal_line_length)

        if str(target_class) in skip_classes:
            logger.info(f'Skipping class {target_class}')
            continue

        logger.info(f'Executing for target class: {target_class}')

        try:
            uris = get_uris(
                target_class,
                settings.SSV_SPARQL_ENDPOINT,
                named_graph=settings.SSV_SPARQL_NAMED_GRAPH,
                username=settings.SSV_SPARQL_USERNAME,
                password=settings.SSV_SPARQL_PASSWORD,
                limit=settings.SSV_SPARQL_QUERY_LIMIT
            )
            # TODO: Remove the 3 lines below which shows the query used to fetch the data.
            # from shacl_sparql_validator.queries import get_validate_construct_query
            # u = [row[0] for row in uris]
            # print(get_validate_construct_query(u))

            df = spark.createDataFrame(uris, schema=['uri'])
            num_of_rows_per_partition = settings.SSV_ROWS_PER_PARTITION
            partition = int(df.count() / num_of_rows_per_partition)
            partition = partition if partition > 0 else 1

            logger.info(f'df.count() = {df.count()} rows per partition: {num_of_rows_per_partition} partition count: {partition}')

            df = df.repartition(partition)

            logger.info(f'Number of partitions: {df.rdd.getNumPartitions()}')

            df.foreachPartition(lambda rows: job(rows, target_class, shapes_graph, job_results))
        except EmptySPARQLResultException:
            logger.info('No data for this class.')
            classes_with_no_data.append(target_class)

    endtime = time.time() - starttime
    logger.info('#' * horizontal_line_length)
    logger.info(f'Spark job completed in {endtime:.2f} seconds.')

    if classes_with_no_data:
        logger.info('Some target classes had no data.')
        for target_class in classes_with_no_data:
            logger.info(f'- {target_class}')

    if classes_with_http_errors:
        logger.info('Some target classes had HTTP errors.')
        for target_class in classes_with_http_errors:
            logger.info(f'- {target_class}')

    logger.info('#' * horizontal_line_length)
    logger.info('Reports from workers')

    error_report = False
    if job_results.value and isinstance(job_results.value, list):
        for result in job_results.value:
            conforms: bool = result[0]
            results_text: str = result[1]

            if not conforms:
                logger.info(f'job_results length: {results_text}')
                error_report = True

    if not error_report:
        logger.info('All classes validated successfully.')
