# SHACL SPARQL Validator

Run SHACL validation against data via a SPARQL endpoint.

## Running on Mac
Set this setting.
```shell
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
```

## Run Spark job locally

See `.env-template` file for a minimum set of things required to run. For the full list of configurable variables, see `settings.py`.

```shell
spark-submit --master local[*] --conf spark.driver.memory=2g --conf spark.executor.memory=2g run.py
```

## Running tests
```shell
# Run inside the tests directory
pytest --cov=shacl_sparql_validator --cov-report html
```
