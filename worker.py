import logging
from typing import Iterable

from pyspark import Accumulator
from pyspark.sql import Row
from rdflib import Graph

import settings
from shacl_sparql_validator import validate

logger = logging.getLogger(__name__)


def job(rows: Iterable[Row], target_class: str, shapes_graph: Graph, job_results: Accumulator):
    rows = list(rows)
    logger.info(f'Received rows: {rows}')
    if rows:
        uris = [row.uri for row in rows]
        logger.info(uris)

        result = validate(
            target_class,
            uris,
            shapes_graph,
            settings.SSV_SPARQL_ENDPOINT,
            settings.SSV_SPARQL_NAMED_GRAPH,
            settings.SSV_SPARQL_USERNAME,
            settings.SSV_SPARQL_PASSWORD
        )

        job_results.add((result[0], result[2]))
