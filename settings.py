from os import environ

try:
    from dotenv import load_dotenv
    load_dotenv()
except:
    pass


#
# Required settings
#

# SHACL shapes definition - file path or URL separated by comma.
# E.g. /Users/uqechuc/TBCMEWorkspace/projects/ontology_tern/docs/tern.ttl,https://w3id.org/tern/ontologies/loc.ttl
SSV_SHACL_SHAPES = environ['SSV_SHACL_SHAPES']
SSV_SHACL_SHAPES = SSV_SHACL_SHAPES.split(',')

# SPARQL endpoint to fetch the data to be validated
SSV_SPARQL_ENDPOINT = environ['SSV_SPARQL_ENDPOINT']
SSV_SPARQL_USERNAME = environ['SSV_SPARQL_USERNAME']
SSV_SPARQL_PASSWORD = environ['SSV_SPARQL_PASSWORD']
SSV_SPARQL_NAMED_GRAPH = environ['SSV_SPARQL_NAMED_GRAPH']


#
# Optional settings with defaults
#

# SHACL shapes definition file format - format type is one of the accepted types by RDFLib.
SSV_SHACL_SHAPES_FORMAT = environ.get('SSV_SHACL_SHAPES_FORMAT', 'turtle')

SSV_SPARQL_QUERY_LIMIT = int(environ.get('SSV_SPARQL_QUERY_LIMIT', None))

# Skip validation - value is a comma-separated set of classes.
SSV_SKIP_CLASSES = environ.get('SSV_SKIP_CLASSES', '')
SSV_SKIP_CLASSES = SSV_SKIP_CLASSES.split(',') if SSV_SKIP_CLASSES else list()

# Spark settings - equivalent to Spark variables with the same name (minus the SSV_ prefix).
SSV_SPARK_MASTER = environ.get('SSV_SPARK_MASTER', 'local[*]')
SSV_SPARK_APP_NAME = environ.get('SSV_SPARK_APP_NAME', 'SHACL SPARQL Validator')
SSV_SPARK_LOG_LEVEL = environ.get('SSV_SPARK_LOG_LEVEL', 'INFO')
SSV_SPARK_DRIVER_MEMORY = environ.get('SSV_SPARK_DRIVER_MEMORY', '1g')

# Spark DataFrame rows per partition.
SSV_ROWS_PER_PARTITION = int(environ.get('SSV_ROWS_PER_PARTITION', '100'))

# Line format length for '#' used in log info output.
SSV_HORIZONTAL_LINE_LENGTH = int(environ.get('SSV_HORIZONTAL_LINE_LENGTH', '80'))
