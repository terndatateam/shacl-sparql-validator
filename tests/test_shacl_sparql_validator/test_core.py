from pytest_mock import MockerFixture
from rdflib import Graph, URIRef, RDF, OWL, RDFS, Literal
from requests import Response
from tern_rdf.namespace_bindings import TERN

from shacl_sparql_validator import fetch
from shacl_sparql_validator.core import exists, graph_values, fetch_triples_recursively, get_node_shape_by_target_class, \
    get_sub_shacl_graph


def test_fetch(mocker: MockerFixture):
    sparql_endpoint = 'http://example.com/sparql'
    mocked_response = Response()
    mocked_response.status_code = 200
    mocked_response.raise_for_status()
    mocker.patch('requests.Session.post', return_value=mocked_response)
    response = fetch(sparql_endpoint=sparql_endpoint)
    assert isinstance(response, Response)


def test_exists():
    g = Graph()
    individual = URIRef('individual-1')
    g.add((individual, RDF.type, OWL.Thing))

    assert exists(g, s=individual, p=RDF.type, o=OWL.Thing)
    assert not exists(g, s=individual, p=RDFS.label, o=None)


def test_graph_values():
    g = Graph()
    individual = URIRef('individual-1')
    g.add((individual, RDF.type, OWL.Thing))
    g.add((individual, RDFS.label, Literal('individual 1')))
    g.add((individual, RDFS.comment, Literal('An individual named 1.')))

    values = graph_values(g, None, None, None)
    assert set(list(values)) == {
        (individual, RDF.type, OWL.Thing),
        (individual, RDFS.label, Literal('individual 1')),
        (individual, RDFS.comment, Literal('An individual named 1.')),
    }


def test_fetch_triples_recursively():
    query_graph = Graph().parse('shapes.ttl', format='turtle')
    result_graph = Graph()
    target_class = URIRef('https://w3id.org/tern/shacl/tern/Text')

    fetch_triples_recursively(query_graph, result_graph, target_class, None, None)
    assert len(result_graph) > 0
    result = result_graph.serialize(format='turtle').decode('utf-8')
    assert result == """@prefix ns1: <http://www.w3.org/ns/shacl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<https://w3id.org/tern/shacl/tern/Text> a rdfs:Class,
        ns1:NodeShape ;
    rdfs:label "Text" ;
    rdfs:subClassOf rdfs:Resource ;
    ns1:closed true ;
    ns1:ignoredProperties ( rdf:type ) ;
    ns1:property [ a ns1:PropertyShape ;
            ns1:maxCount 1 ;
            ns1:minCount 1 ;
            ns1:or ( [ ns1:datatype xsd:string ] [ ns1:datatype xsd:langString ] ) ;
            ns1:path rdf:value ] ;
    ns1:targetClass <https://w3id.org/tern/ontologies/tern/Text> .

"""


def test_get_node_shape_by_target_class():
    target_class = TERN.Text
    shacl_graph = Graph().parse('shapes.ttl', format='turtle')
    result_graph = get_node_shape_by_target_class(target_class, shacl_graph)
    result = result_graph.serialize(format='turtle').decode('utf-8')
    assert result == """@prefix ns1: <http://www.w3.org/ns/shacl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<https://w3id.org/tern/shacl/tern/Text> a rdfs:Class,
        ns1:NodeShape ;
    rdfs:label "Text" ;
    rdfs:subClassOf rdfs:Resource ;
    ns1:closed true ;
    ns1:ignoredProperties ( rdf:type ) ;
    ns1:property [ a ns1:PropertyShape ;
            ns1:maxCount 1 ;
            ns1:minCount 1 ;
            ns1:or ( [ ns1:datatype xsd:string ] [ ns1:datatype xsd:langString ] ) ;
            ns1:path rdf:value ] ;
    ns1:targetClass <https://w3id.org/tern/ontologies/tern/Text> .

"""


def test_get_sub_shacl_graph():
    target_class = 'https://w3id.org/tern/ontologies/tern/Text'
    shacl_graph = Graph().parse('shapes.ttl', format='turtle')
    result_graph = get_sub_shacl_graph(target_class, shacl_graph)
    result = result_graph.serialize(format='turtle').decode('utf-8')
    assert result == """@prefix ns1: <http://www.w3.org/ns/shacl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<https://w3id.org/tern/shacl/tern/Text> a rdfs:Class,
        ns1:NodeShape ;
    rdfs:label "Text" ;
    rdfs:subClassOf rdfs:Resource ;
    ns1:closed true ;
    ns1:ignoredProperties ( rdf:type ) ;
    ns1:property [ a ns1:PropertyShape ;
            ns1:maxCount 1 ;
            ns1:minCount 1 ;
            ns1:or ( [ ns1:datatype xsd:string ] [ ns1:datatype xsd:langString ] ) ;
            ns1:path rdf:value ] ;
    ns1:targetClass <https://w3id.org/tern/ontologies/tern/Text> .

"""
