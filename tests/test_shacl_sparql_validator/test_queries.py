from shacl_sparql_validator.queries import get_uris_query, get_validate_construct_query


def test_get_uris_query():
    target_class = 'https://w3id.org/tern/ontologies/tern/Text'
    named_graph = 'http://example.com/graph-1'
    query = get_uris_query(target_class, named_graph=named_graph, limit=100)

    assert query == """
SELECT ?uri

FROM <http://example.com/graph-1>

WHERE {
    ?uri a <https://w3id.org/tern/ontologies/tern/Text>
}
LIMIT 100"""


def test_get_validate_construct_query():
    uris = ['http://example.com/1', 'http://example.com/2']
    get_validate_construct_query(uris)
