import pytest
from rdflib import Graph

from shacl_sparql_validator.exceptions import InvalidShapesException
from shacl_sparql_validator.utils import validate_shapes


def test_validate_shapes_positive():
    data_graph_string = """
        @prefix sh: <http://www.w3.org/ns/shacl#> .
        @prefix tern-shape: <https://w3id.org/tern/shacl/tern/> .
        tern-shape:FeatureOfInterest
          a sh:NodeShape ;
          sh:targetClass <https://w3id.org/tern/ontologies/tern/FeatureOfInterest> ;
        .
    """
    data_graph = Graph().parse(data=data_graph_string, format='turtle')
    validate_shapes(data_graph)


def test_validate_shapes_negative():
    data_graph_string = """
        @prefix sh: <http://www.w3.org/ns/shacl#> .
        @prefix tern-shape: <https://w3id.org/tern/shacl/tern/> .
        tern-shape:FeatureOfInterest
          a sh:NodeShape ;
        .
    """
    data_graph = Graph().parse(data=data_graph_string, format='turtle')
    with pytest.raises(InvalidShapesException):
        validate_shapes(data_graph)
