from typing import Any

import pytest
from pytest_mock import MockerFixture
from rdflib import Graph
from rdflib.namespace import TIME
from requests import Response
from tern_rdf.namespace_bindings import TERN

from shacl_sparql_validator import get_target_classes, validate, get_uris, EmptySPARQLResultException


def test_get_uris(mocker: MockerFixture):
    target_class = 'https://w3id.org/tern/ontologies/tern/Text'
    sparql_endpoint = 'http://example.com/sparql'

    class SPARQLJSONResponse(Response):
        def json(self, **kwargs) -> Any:
            return {'results': {
                'bindings': [
                    {
                        'uri': {
                            'value': 'http://example.com/1'
                        }
                    },
                    {
                        'uri': {
                            'value': 'http://example.com/2'
                        }
                    }
                ]
            }}

    mocked_response = SPARQLJSONResponse()
    mocked_response.status_code = 200
    mocked_response.raise_for_status()
    mocker.patch('requests.Session.post', return_value=mocked_response)

    uris = get_uris(target_class, sparql_endpoint)
    assert set(uris) == {
        ('http://example.com/1',),
        ('http://example.com/2',),
    }


def test_get_uris_raise_emptysparqlresultsexception(mocker: MockerFixture):
    target_class = 'https://w3id.org/tern/ontologies/tern/Text'
    sparql_endpoint = 'http://example.com/sparql'

    class SPARQLJSONResponse(Response):
        def json(self, **kwargs) -> Any:
            return {'results': {
                'bindings': []
            }}

    mocked_response = SPARQLJSONResponse()
    mocked_response.status_code = 200
    mocker.patch('requests.Session.post', return_value=mocked_response)

    with pytest.raises(EmptySPARQLResultException):
        get_uris(target_class, sparql_endpoint)


def test_validate(mocker: MockerFixture):
    target_class = 'https://w3id.org/tern/ontologies/tern/Site'
    sparql_endpoint = 'http://example.com/sparql'
    uris = [
        'http://linked.data.gov.au/dataset/ausplots/site-waacoo0007',
        'http://linked.data.gov.au/dataset/ausplots/site-waacoo0005',
        'http://linked.data.gov.au/dataset/ausplots/site-waacoo0004',
    ]
    shacl_graph = Graph().parse('shapes.ttl', format='turtle')

    class TurtleResponse(Response):
        @property
        def text(self) -> str:
            import os
            print('hello')
            print(os.getcwd())
            with open('test_shacl_sparql_validator/test_validate_turtle_response_data.ttl', 'r') as f:
                return f.read()

    mocked_response = TurtleResponse()
    mocked_response.status_code = 200
    mocker.patch('requests.Session.post', return_value=mocked_response)

    validate(target_class, uris, shacl_graph, sparql_endpoint)


def test_get_target_classes():
    g = Graph().parse('shapes.ttl', format='turtle')
    target_classes = get_target_classes(g)
    target_classes = set(list(target_classes))
    expected = {
        TERN.Attribute,
        TERN.Boolean,
        TERN.Concept,
        TERN.Count,
        TERN.Date,
        TERN.DateTime,
        TERN.FeatureOfInterest,
        TERN.IRI,
        TIME.Instant,
        TERN.Instrument,
        TERN.ObservableProperty,
        TERN.Observation,
        TERN.Percent,
        TERN.PercentRange,
        TERN.QuantitativeMeasure,
        TERN.QuantitativeRange,
        TERN.Result,
        TERN.Sample,
        TERN.Sampling,
        TERN.Site,
        TERN.SiteVisit,
        TERN.Text,
    }
    assert target_classes == expected
