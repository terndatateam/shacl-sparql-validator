from typing import List

from jinja2 import Template


def get_uris_query(target_class: str, named_graph: str = None, limit: int = None):
    template = """
SELECT ?uri
{% if named_graph %}
FROM <{{ named_graph }}>
{% endif %}
WHERE {
    ?uri a <{{ target_class }}>
}
{% if limit %}LIMIT {{ limit }}{% endif %}
"""
    return Template(template).render(target_class=target_class, named_graph=named_graph, limit=limit)


def get_validate_construct_query(uris: List[str], named_graph: str = None):
    template = """
CONSTRUCT {
  ?uri ?p ?o .
  ?o a ?class .
}
{% if named_graph %}
FROM <{{ named_graph }}>
{% endif %}
WHERE {
  VALUES (?uri) {
    {% for uri in uris %}(<{{ uri }}>)\n\t{% endfor %}
  }
    ?uri ?p ?o .
    OPTIONAL { ?o a ?class }
}
"""
    return Template(template).render(uris=uris, named_graph=named_graph)
