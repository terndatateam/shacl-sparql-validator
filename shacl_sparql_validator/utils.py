from rdflib import Graph, URIRef, RDF, SH, RDFS
from pyshacl import validate

from shacl_sparql_validator.exceptions import InvalidShapesException


def is_node_shape(graph: Graph, uri: URIRef):
    for _ in graph.triples((uri, RDF.type, SH.NodeShape)):
        return True
    return False


def process_shapes(graph: Graph):
    node_shapes = list()

    for s, _, o in graph.triples((None, RDF.type, None)):
        if is_node_shape(graph, s) or str(o).startswith(SH):
            pass
        else:
            for s, p, o in graph.triples((s, None, None)):
                graph.remove((s, p, o))

    for s, _, _ in graph.triples((None, RDF.type, SH.NodeShape)):
        node_shapes.append(s)
        graph.add((s, SH.targetClass, s))

        for s, p, o in graph.triples((s, None, None)):
            graph.remove((s, p, o))
            graph.add((URIRef(s + '/Shape'), p, o))

    for s, p, o in graph.triples((None, RDFS.subClassOf, None)):
        graph.remove((s, p, o))


def validate_shapes(data_graph: Graph):
    """Ensure all NodeShapes have sh:targetClass."""
    shape_string = """
        @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
        @prefix tern-shape: <https://w3id.org/tern/shacl/tern/> .
        @prefix sh: <http://www.w3.org/ns/shacl#> .
        @prefix tern-shape: <https://w3id.org/tern/shacl/tern/> .
        
        tern-shape:NodeShape a sh:NodeShape ;
          sh:targetClass sh:NodeShape ;
          sh:property [
            a sh:PropertyShape ;
            sh:path sh:targetClass ;
            sh:minCount 1 ;
            sh:maxCount 1 ;
          ] ;
        .
    """

    shacl_graph = Graph().parse(data=shape_string, format='turtle')
    valid, _, message = validate(data_graph, shacl_graph=shacl_graph, abort_on_error=True)

    if not valid:
        raise InvalidShapesException(message)
