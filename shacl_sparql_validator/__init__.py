import logging
from typing import Tuple, List, Generator

from rdflib import Graph
from rdflib.namespace import SH, RDF
from rdflib.term import URIRef
from pyshacl import validate as shacl_validate

from shacl_sparql_validator.core import fetch, get_sub_shacl_graph
from shacl_sparql_validator.exceptions import EmptySPARQLResultException
from shacl_sparql_validator.queries import get_uris_query, get_validate_construct_query

logger = logging.getLogger(__name__)


def get_uris(
        target_class: str,
        sparql_endpoint: str,
        named_graph: str = None,
        username: str = None,
        password: str = None,
        limit: int = None,
) -> List[Tuple[str]]:
    """Given a SHACL target class, a SPARQL endpoint, and optionally a named graph, authentication credentials,
    fetch a list of tuples containing URIs as a string. Optionally pass a limit value to limit the number of results.
    """
    query = get_uris_query(target_class, named_graph, limit)

    params = {
        'query': query,
    }
    headers = {
        'accept': 'application/sparql-results+json'
    }

    response = fetch(
        sparql_endpoint,
        auth=(username, password) if username and password else None,
        params=params,
        headers=headers
    )
    data = response.json()['results']['bindings']
    logger.info(f'SPARQL data result size: {len(data)}')

    if len(data) > 0:
        uris = [(uri['uri']['value'],) for uri in data]
        return uris
    else:
        raise EmptySPARQLResultException(f'Failed to retrieve results for query: {query}')


def validate(
        target_class: str,
        uris: List[str],
        shacl_graph: Graph,
        sparql_endpoint: str,
        named_graph: str = None,
        username: str = None,
        password: str = None,
):
    """Given a SHACL target class, a list of URIs, an RDFLib Graph with the SHACL
    node shapes and a SPARQL endpoint, fetch the data and validate it using PySHACL.
    """
    query = get_validate_construct_query(uris, named_graph)

    params = {
        'query': query
    }
    headers = {
        'accept': 'text/turtle'
    }

    response = fetch(
        sparql_endpoint,
        auth=(username, password) if username and password else None,
        params=params,
        headers=headers
    )
    data = response.text

    data_graph = Graph().parse(data=data, format='turtle')
    sub_shacl_graph = get_sub_shacl_graph(target_class, shacl_graph)
    sub_shacl_graph.serialize('sub_shape.ttl', format='turtle')

    result = shacl_validate(data_graph, shacl_graph=sub_shacl_graph)
    return result


def get_target_classes(shapes_graph: Graph, skip_classes: List[str] = None) -> Generator[URIRef, None, None]:
    node_shapes = shapes_graph.subjects(RDF.type, SH.NodeShape)
    for node_shape in node_shapes:
        target_class = shapes_graph.value(node_shape, SH.targetClass)
        if target_class is not None:
            if skip_classes is not None and target_class in skip_classes:
                # Skip the current target_class as it is in skip_classes.
                continue
            yield target_class
