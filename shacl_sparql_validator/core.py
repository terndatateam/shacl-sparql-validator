from typing import Tuple, List

from jinja2 import Template
from rdflib import URIRef, Graph, SH
from rdflib.term import Node
from requests import Response
from tern_rdf.utils import create_session


def fetch(sparql_endpoint: str, auth: Tuple[str, str] = None, params: dict = None, headers: dict = None) -> Response:
    http = create_session()
    r = http.post(sparql_endpoint, data=params, headers=headers, auth=auth)
    r.raise_for_status()
    return r


def exists(graph: Graph, s: Node = None, p: Node = None, o: Node = None) -> bool:
    for _, _, _ in graph.triples((s, p, o)):
        return True
    return False


def graph_values(query_graph: Graph, s: Node = None, p: Node = None, o: Node = None):
    for _s, _, _ in query_graph.triples((s, p, o)):
        for _, _p, _o in query_graph.triples((_s, None, None)):
            yield _s, _p, _o


def fetch_triples_recursively(query_graph: Graph, result_graph: Graph, s: Node = None, p: Node = None, o: Node = None):
    for _s, _p, _o in query_graph.triples((s, p, o)):
        result_graph.add((_s, _p, _o))
        fetch_triples_recursively(query_graph, result_graph, _o, None, None)


def get_node_shape_by_target_class(target_class: URIRef, graph: Graph) -> Graph:
    result_graph = Graph()
    result = graph_values(graph, None, SH.targetClass, target_class)

    for s, p, o in result:
        fetch_triples_recursively(graph, result_graph, s, p, o)

    return result_graph


def get_sub_shacl_graph(target_class: str, shacl_graph: Graph) -> Graph:
    target_class = URIRef(target_class)
    if exists(shacl_graph, o=target_class):
        return get_node_shape_by_target_class(target_class, shacl_graph)
    else:
        # TODO: SHACL spec defines that sh:targetClass may not always be available,
        #   and that if any sh:NodeShape also has an rdfs:Class, then the rdfs:Class is
        #   equivalent to sh:targetClass.
        raise NotImplementedError()
